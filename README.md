# gline-mark

## Project setup
```
npm i
```

### Compiles and hot-reloads for development
```
npm run hmr
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### Tested in browsers
```
Google Chrome Version 59.0.3071.86 (Official Build)

Firefox 46.0.1 
```
### Примечания
```
форма допускает отправку пустых данных, слайдер для отзывов не реализован
