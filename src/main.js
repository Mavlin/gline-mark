'use strict';
import "./assets/style.scss";
import * as Tools from './js/Tools.js';
import * as popUp from './js/PopUp.js';
import 'jquery.maskedinput/src/jquery.maskedinput.js';

let $2 = Tools.getSel,
    actions = {
        openPopUp: popUp.openPopUp,
        closePopUp: popUp.closePopUp,
        closePopUpClosest: popUp.closePopUpClosest,
        closeAllPopUp: popUp.closeAllPopUp,
        sendMessage: sendMessage,
    },
    content = $2('body');


content.addEventListener('click',(e)=>{
    e.preventDefault();
    e.stopPropagation();
    let target, act, action, el;
    if (e.target) {
        // debugger
        target = e.target;//
        if (target){
            el = target.closest('[data-action]');
            if (el) {
                action = el.dataset.action;
                if (action) {
                    act = action.split(':');
                    if (act) {
                        actions[act[0]](act[1] === 'this' ? e : act[1])
                    }
                }
            }
        }
    }
});

function sendMessage(arg) {
    let el = $(arg.target.closest('form'));
    jQuery(function($) {
        $.ajax({
            url: "https://api.myjson.com/bins",
            type: "POST",
            data: JSON.stringify(el.serializeArray()),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                console.log(data, jqXHR)
                console.log('Data is sended!')
                actions.openPopUp("gr23")
            }
        });
    })
}

jQuery(function($){
    $("#phone").mask("+7 999 999 9999",{completed:function(){
        console.log(this.val())
        if (this.val()==='+7 999 999 9999'){
            this.addClass('err')
            $('div.err').removeClass('none')
            return
        }
        this.addClass('completed')
    }});
});

