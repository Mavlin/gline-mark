'use strict';

function gid( a ){
    return document.getElementById( a )
}

function getSel( a ){
    return document.querySelector( a )
}

function getSels( a ){
    return document.querySelectorAll( a )
}

function getEl(el){
    if ( !(el instanceof HTMLElement) ){
        return document.querySelector( el );
    }
}

export { gid, getSel, getSels, getEl }
