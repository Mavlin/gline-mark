'use strict';
import * as Tools from './Tools.js';

let $ = Tools.getSel,
    $$ = Tools.getSels,
    gid = Tools.gid,
    dialogBoxes = $$('.dialog'),
    overlay = $('.overlay'),
    openPopUps = new Map();

// Close the dialog - press escape key // key#27
document.addEventListener('keyup', function(e) {
    if (e.keyCode === 27) {
        let cntOpenPopUps = openPopUps.size, i = 0;
        openPopUps.forEach((val, key, set)=>{
            if (++i === cntOpenPopUps){
                closePopUp(key);
                set.delete(key)
            }
        })
    }
});

// Close the dialog - click outside
function openPopUp(el) {
    let tpl = gid('tpl-'+el), elId = gid(el);
    if (tpl) {
        elId.innerHTML = tpl.innerHTML
    }
    elId.style.display = 'block';
    setTimeout(() => {
        elId.classList.add('active');
        overlay.classList.add('active');
        openPopUps.set(el, 1); // list of opened popup windows
        window.scrollTo(0,0)
    }, 20);
    setTimeout(() => {
        let form = elId.querySelector('form'), field;
        if (form) {
            field = form.querySelector('[autofocus="autofocus"]');
            if (field) {
                field.focus()
            }
        }
    }, 200)
}

function closePopUp(e) {
    if (e.target && e.target.classList.contains('active')){
        closePopUpClosest(e)
    }
}

function closePopUpClosest(e) {
    let dialog;
    if (e.target){
        dialog = e.target.closest('.dialog')
    } else {
        dialog = gid(e)
    }
    if (dialog) {
        dialog.classList.remove('active');
        openPopUps.delete(dialog.getAttribute('id'));
        setTimeout(function () {
            dialog.style.display = 'none'
        }, 500);
        if (!$$('.dialog.active').length) {
            overlay.classList.remove('active')
        }
    }
}

function closeAllPopUp() {
    if (overlay) {
        overlay.classList.remove('active');
    }
    openPopUps.clear();
    for (let item in dialogBoxes){
        if (dialogBoxes.hasOwnProperty(item)) {
            dialogBoxes[item].classList.remove('active');
            setTimeout(function () {
                dialogBoxes[item].style.display='none'
            },500)
        }
    }
}

export { openPopUp, closePopUp, closePopUpClosest, closeAllPopUp }

