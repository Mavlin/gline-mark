const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const webpack = require('webpack');
const merge = require('webpack-merge');

// console.log(process.env)
console.log(process.env.NODE_ENV);
const config = require('./config');
if ( !process.env.NODE_ENV ){
    process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}
console.log(process.env.NODE_ENV);


function resolve (dir) {
    return path.join(__dirname, '.', dir)
}

module.exports = ({
    mode: process.env.NODE_ENV,
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: false,
                    },
                },
                cache: true,
                parallel: true,
                extractComments: true,
                // sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    entry: {
        index:'./src/main.js',
    },
    output: {
        filename: 'js/[name].js',
        path: resolve('dist'),
    },
    devtool: (process.env.NODE_ENV==='development')?'inline-source-map':'',
    devServer: {
        historyApiFallback: true
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.pug$/,
                include: resolve('src'),
                use: [
                    {
                        loader:'html-loader',
                        options:{
                            minimize:true,
                        }
                    },
                    'pug-html-loader?-pretty',
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: ('[name].[ext]')
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: ('fonts/[name].[ext]')
                }
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "style.css",
            chunkFilename: "[id].css"
        }),
        new CleanWebpackPlugin(
            ['dist'], { verbose: true, beforeEmit: true, dry: false} // dry: false - затирает
        ),
        new HtmlWebpackPlugin({
            template: './src/tpl/index.pug',
            options: {
                minimize: true,
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new ScriptExtHtmlWebpackPlugin({
                custom:[
                    {
                        test: 'index.js',
                        attribute: 'type',
                        value: 'application/javascript',
                        defaultAttribute: 'defer'
                    },
                    {
                        test: /\.js$/,
                        attribute: 'type',
                        value: 'text/javascript',
                        defaultAttribute: 'defer'
                    },
                ]
            }
        ),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
        })
    ]
});
 